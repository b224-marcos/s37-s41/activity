const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");


// Routes
/*// Route for creating a course
router.post("/addCourse", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
});
*/

//Session 39 - Activity - Solution 1
router.post('/addCourse', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});


//Session 39 - Activity Solution 2
/*router.post("/addCourse", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})*/

// Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});


// Route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});


// Route for retrieving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});


// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});




router.put("/archive/:courseId", auth.verify, (req, res)=>{

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	
	courseController.archiveCourse(req.params,req.body, isAdmin).then( resultFromController => res.send(resultFromController));
})







module.exports = router;