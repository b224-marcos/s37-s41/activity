
const Course = require("../models/Course");
const User = require('../models/User');

// Controller Functions:

// Creating a new course
module.exports.addCourse = (reqBody) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	// Saves the created object to our database
	return newCourse.save().then((course, error) => {

		// Course creation failed
		if(error) {

			return false

		// Course creation successful
		} else {

			return true
		}
	})
}


//Activity Solution 2
/*module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}*/


// Retriving All Courses
module.exports.getAllCourses = (data) => {

	if(data.isAdmin) {
		return Course.find({}).then(result => {

			return result
		})
	} else {
		return false // "You are not an Admin."
	}
};


// Retrieve All Active Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	})
};


// Retrieve a Specific Course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result
	})
};


// Update a Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {

		console.log(updatedCourse)
		if(error) {
			return false
		} else {
			return true
		}
	}) 

};



module.exports.archiveCourse = (reqParams, reqBody, isAdmin) => {

	 if(isAdmin){
	 	let updateStatus = {
	 		isActive: reqBody.isActive
	 	}
	 	return Course.findByIdAndUpdate(reqParams.courseId, updateStatus).then((statusUpdated, error) => {
	 		if(error){
	 			return false
	 		} else {
	 			return true
	 		}
	 	});

	 } 
};



